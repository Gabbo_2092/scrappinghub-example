from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import re
import time

class slum_crawler:

	def __init__(self): 
		self.driver = webdriver.PhantomJS(service_log_path='/var/www/html/crawler/log/phantomlog.log')

	def close_driver(self):
		self.driver.quit()

	def getHTML(self,url,juzgado,date):
		print "Descargando informacion..."
		try:
			#driver = webdriver.PhantomJS()
			#driver = webdriver.Firefox()
			#self.driver = webdriver.PhantomJS(service_log_path='/var/www/html/alpha/selenium/log/phantomlog.log')
			self.driver.get(url)
			option = self.driver.find_element_by_css_selector('option[value="'+str(juzgado)+'"]') #seleccionar el juzgado
			option.click()
			option = self.driver.find_element_by_css_selector('option[value="1"]') #seleccionar la opcion de buscar por fecha
			option.click()
			dateField = self.driver.find_element_by_id("Fecha")
			dateField.clear();
			dateField.send_keys(date);
			btn = self.driver.find_element_by_css_selector('input.boton.btnBuscar')
			btn.click()
			
			container = self.driver.find_element_by_css_selector('div#container')
		except NoSuchElementException as e:
			self.driver.save_screenshot('screenshot.png')
			raise Exception("error no se encontro un elemento")
			
		html = self.preProcessHTML(container.get_attribute('innerHTML').encode('utf-8'))
		
		return html

	

	def get_UnprocessedHTML(self,url):
		print "Descargando informacion..."
		try:
			driver = webdriver.PhantomJS()
			#driver = webdriver.PhantomJS(service_log_path='/home/alpha/phantomlog.log')
			driver.get(url)
			btn = driver.find_element_by_css_selector('input.boton.btnBuscar')
			btn.click()
			container = driver.find_element_by_css_selector('div#container')
		except NoSuchElementException as e:
			raise Exception("error al obtener el html")
		return container.get_attribute('innerHTML')

	def preProcessHTML(self,html):
		return re.sub('\s\s+|\t+|\n',' ',html)

	def removeCharacters(self,s):
		s=re.sub("'","",s)
		s=re.sub("</a><br>"," ",s)
		s=re.sub("-"," ",s)

		s=re.sub("\s\s+"," ",s);#siempre a lo ultimo
		return s

	"""
	recive el html descargado por selenium
	@returns: una lista con dos elementos. El primero son las secretarias que se encontraron,
	el segundo un arreglo, donde cada elemento del arreglo es un arreglo con los acuerdos encontrados.
	O un mensaje de error o advertencia
	if
	"""
	def getData(self,html):
		try:
			print "Procesando informacion..."

			secretarias = re.findall('<h4 class="subtitulo">(.*?)</h4>',html)
			ac_tables = re.findall('<table class="table table-hover table-condensed">(.*?)</table>',html)
			if len(secretarias) != len(ac_tables) or len(secretarias) == 0 or len(ac_tables) == 0:
				return "ERROR: Ocurrio un error al scrappear la lista de acuerdos"
			
			acuerdos = [[] for _ in range(len(ac_tables))]
			for i in range(len(ac_tables)): 
				if re.search("No se encontraron acuerdos",ac_tables[i]) != None:
						acuerdos[i].append([])
						continue
				rows = re.findall('<tr>(.*?)</tr>',ac_tables[i])
				for j in range(len(rows)): #cada acuerdo
					asunto = re.findall('<td class="text-center"><a href=.*?>(.*?)<\/td>',rows[j])
					if len(asunto)==0:
						continue
					new_ac = [ac for ac in re.findall('<td class="text-justify">(.*?)</td>',rows[j])]
					new_ac.insert(0,self.removeCharacters(asunto[0]))
					acuerdos[i].append(new_ac)
			return [secretarias,acuerdos]
		except Exception as e:
			raise

	def execute(self,url,juzgado,date):
		html = self.getHTML(url,juzgado,date)
		if html == ' ':
			return "No se encontro nada"
		return self.getData(html)
